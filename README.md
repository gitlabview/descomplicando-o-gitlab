# Descomplicando o GitLab

Treinamento descomplicando GitLab criado ao vivo na Twitch

### Day-1
- Entendemos o que é o Git
- Entendemos o que é o GitLab
- Como criar um grupo no GitLab
- Como criar um repositorio git
- Aprendemos os comandos básicos para a manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o merger na Master/Main
